package com.palmwin;

/**
 * 适配器中的点击监听器
 *
 */
public interface IAdapterClickListener {

    /**
     * 适配器里的点击事件
     * @param id
     * @param position
     */
    public void adapterClick(int id, int position, int position2);

}

