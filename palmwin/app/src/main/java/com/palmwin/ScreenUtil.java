package com.palmwin;

import android.content.Context;
import android.util.DisplayMetrics;
import android.util.TypedValue;

/**
 * 屏幕分辨率工具类
 * created on 2017/02/07
 */
public class ScreenUtil {

	/**
	 * 屏幕宽高
	 * @return
	 */
	private static int[] getScreenSize(Context context) {
		DisplayMetrics dm = context.getResources().getDisplayMetrics();
		int screenWidth = dm.widthPixels;
		int screenHeight = dm.heightPixels;
		return new int[] { screenWidth, screenHeight };
	}

	/**
	 * 获取手机屏幕的宽度
	 */
	public static int getScreenWidth(Context context) {
		int screen[] = getScreenSize(context);
		return screen[0];
	}

	/**
	 * 获取手机屏幕的高度
	 */
	public static int getScreenHeight(Context context) {
		int screen[] = getScreenSize(context);
		return screen[1];
	}

	/**
	 * 根据手机分辨率将dp转为px单位
	 */
	public static int dip2px(float dpValue, Context context) {
		final float scale = context.getResources().getDisplayMetrics().density;
		return (int) (dpValue * scale + 0.5f);
	}

	/**
	 * 根据手机的分辨率从 px(像素) 的单位 转成为 dp
	 */
	public static int px2dip(float pxValue, Context context) {
		final float scale = context.getResources().getDisplayMetrics().density;
		return (int) (pxValue / scale + 0.5f);
	}

	public static int getDip(Context context, int arg) {
		return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, arg, context.getResources().getDisplayMetrics());
	}


}
