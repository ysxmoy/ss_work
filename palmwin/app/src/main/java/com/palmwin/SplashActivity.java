package com.palmwin;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.ImageView;

import com.palmwin.palmwin.R;
import com.palmwin.ui.activity.MainActivity;
import com.palmwin.ui.base.BaseActivity;

import butterknife.BindView;


public class SplashActivity extends BaseActivity implements Animation.AnimationListener {


    private static final String TAG = SplashActivity.class.getSimpleName();

    @BindView(R.id.imageView)
    ImageView imageView;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        if ((getIntent().getFlags() & Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT) != 0) {
            finish();
            return;
        }
        super.onCreate(savedInstanceState);
    }

    @Override
    public int getLayoutResId() {
        return R.layout.activity_splash;
    }

    @Override
    public String setTitle() {
        return "";
    }

    @Override
    public boolean isShowBack() {
        return false;
    }

    @Override
    public boolean isShowToolbar() {
        return false;
    }


    @Override
    public void initViews() {
        AlphaAnimation animation = new AlphaAnimation(1.0f, 1.0f);
        animation.setFillAfter(true);
        animation.setDuration(2000);
        animation.setAnimationListener(this);
        imageView.startAnimation(animation);
    }

    @Override
    public void onAnimationStart(Animation animation) {

    }


    @Override
    public void onAnimationEnd(Animation animation) {
        startActivity(new Intent(SplashActivity.this, MainActivity.class));
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        finish();
    }

    @Override
    public void onAnimationRepeat(Animation animation) {
    }


}
