package com.palmwin.adapter;

import android.content.Context;
import android.support.annotation.IntRange;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

/**
 * Created by wxx@diandianwifi.com
 */

public abstract class BaseRecyclerViewAdapter<T, K extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<K>  {

    protected LayoutInflater mLayoutInflater;
    protected Context mContext;
    private List<T> datas;


    public void setDatas(List<T> datas) {
        this.datas = datas;
    }

    public BaseRecyclerViewAdapter(Context context, List<T> datas) {
        this.datas = datas;
        this.mContext = context;
        mLayoutInflater = LayoutInflater.from(context);
    }

    @Nullable
    public T getItem(@IntRange(from = 0) int position) {
        if (position < datas.size())
            return datas.get(position);
        else
            return null;
    }

    @Override
    public K onCreateViewHolder(ViewGroup parent, int viewType) {
        return createVH(parent, viewType);
    }

    @Override
    public void onBindViewHolder(K holder, int position) {
        convert(holder, position);
    }

    @Override
    public int getItemCount() {
        return datas == null ? 0 : datas.size();
    }

    protected abstract K createVH(ViewGroup parent, int viewType);

    protected abstract void convert(K holder, int position);
}
