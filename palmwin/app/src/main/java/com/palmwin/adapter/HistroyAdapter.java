package com.palmwin.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.palmwin.bean.HistoryBean;
import com.palmwin.palmwin.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;



public class HistroyAdapter extends BaseRecyclerViewAdapter<HistoryBean, HistroyAdapter.ViewHolder> {


    private  Context mContext;

    public HistroyAdapter(Context context, List<HistoryBean> planTypeList) {
        super(context, planTypeList);
        mContext = context;
    }


    @Override
    public ViewHolder createVH(ViewGroup parent, int viewType) {
        return new ViewHolder(mLayoutInflater.inflate(R.layout.item_history, parent, false));
    }

    @Override

    protected void convert(ViewHolder holder, int position) {
        HistoryBean item = getItem(position);
//        Glide.with(mContext).applyDefaultRequestOptions(mPlaceholder).load(HeadImageUtil.getPicUrl(item.getBackgroundImagePath())).into(holder.imageView);
        holder.tvTime.setText(item.getName());
    }


    static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvTime)
        TextView tvTime;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

    }

}
