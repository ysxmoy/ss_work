package com.palmwin.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.ViewGroup;

import java.util.List;



public class MyFragmentAdapter extends FragmentStatePagerAdapter {

    private List<String> list_Title;
    private List<Fragment> list_Fragment;

    public MyFragmentAdapter(FragmentManager fm, List<String> list_Title, List<Fragment> list_Fragment) {
        super(fm);
        this.list_Title = list_Title;
        this.list_Fragment = list_Fragment;
    }

    public void setList_Title(List<String> list_Title) {
        this.list_Title = list_Title;
    }

    @Override public Fragment getItem(int position) {
        return list_Fragment.get(position);
    }

    @Override public int getCount() {
        return list_Title.size();
    }

    /**
     * 重写此方法,防止被回收
     */
    @Override public void destroyItem(ViewGroup container, int position, Object object) {
    }

    /**
     * @param position
     * @return
     */

    @Override public CharSequence getPageTitle(int position) {
        return list_Title.get(position);
    }

}