package com.palmwin.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.palmwin.bean.HistoryBean;
import com.palmwin.palmwin.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class PlanDetailAdapter extends BaseRecyclerViewAdapter<HistoryBean, PlanDetailAdapter.ViewHolder> {


    private  Context mContext;

    public PlanDetailAdapter(Context context, List<HistoryBean> planTypeList) {
        super(context, planTypeList);
        mContext = context;
    }


    @Override
    public ViewHolder createVH(ViewGroup parent, int viewType) {
        return new ViewHolder(mLayoutInflater.inflate(R.layout.item_plan_detail, parent, false));
    }

    @Override

    protected void convert(ViewHolder holder, int position) {
        HistoryBean item = getItem(position);
//        Glide.with(mContext).applyDefaultRequestOptions(mPlaceholder).load(HeadImageUtil.getPicUrl(item.getBackgroundImagePath())).into(holder.imageView);
        holder.name_tv.setText(item.getName());
    }


    static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.name_tv)
        TextView name_tv;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

    }

}
