package com.palmwin.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.palmwin.IAdapterClickListener;
import com.palmwin.bean.HistoryBean;
import com.palmwin.palmwin.R;
import com.zhy.view.flowlayout.FlowLayout;
import com.zhy.view.flowlayout.TagAdapter;
import com.zhy.view.flowlayout.TagFlowLayout;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class PlanSelectAdapter extends BaseRecyclerViewAdapter<HistoryBean, PlanSelectAdapter.ViewHolder> {


    private Context mContext;
    private IAdapterClickListener mIAdapterClickListener;

    public void setIAdapterClickListener(IAdapterClickListener IAdapterClickListener) {
        mIAdapterClickListener = IAdapterClickListener;
    }

    public PlanSelectAdapter(Context context, List<HistoryBean> planTypeList) {
        super(context, planTypeList);
        mContext = context;
    }


    @Override
    public ViewHolder createVH(ViewGroup parent, int viewType) {
        return new ViewHolder(mLayoutInflater.inflate(R.layout.select_plan_item, parent, false));
    }

    @Override

    protected void convert(final ViewHolder holder, final int position) {
        HistoryBean item = getItem(position);
//        Glide.with(mContext).applyDefaultRequestOptions(mPlaceholder).load(HeadImageUtil.getPicUrl(item.getBackgroundImagePath())).into(holder.imageView);
        holder.name_tv.setText(item.getName());
        ArrayList<String> mRandomList = new ArrayList<>();
        mRandomList.add("万位定码");
        mRandomList.add("千位定码");
        mRandomList.add("百位定码");
        mRandomList.add("十位定码");
        mRandomList.add("个位定码");
        mRandomList.add("万位杀码");
        mRandomList.add("千位杀码");
        mRandomList.add("百位杀码");
        mRandomList.add("十位杀码");
        mRandomList.add("个位杀码");
        final TagAdapter<String> tagAdapter = new TagAdapter<String>(mRandomList) {
            @Override
            public View getView(FlowLayout parent, int position1, String s) {
                TextView tv = (TextView) LayoutInflater.from(mContext).inflate(R.layout.tab_layout, holder.flowlayout, false);
                tv.setTextColor(mContext.getResources().getColor(R.color.color_black));
                tv.setText(s);
                return tv;
            }

            @Override
            public void onSelected(int pos, View view) {
                TextView view1 = (TextView) view;
                view1.setTextColor(mContext.getResources().getColor(R.color.colorAccent));
                if (mIAdapterClickListener != null) {
                    mIAdapterClickListener.adapterClick(1, pos,position);
                }
            }

            @Override
            public void unSelected(int pos, View view) {
                TextView view1 = (TextView) view;
                view1.setTextColor(mContext.getResources().getColor(R.color.color_black));
                if (mIAdapterClickListener != null) {
                    mIAdapterClickListener.adapterClick(0,pos, position);
                }
            }
        };
        holder.flowlayout.setAdapter(tagAdapter);


    }


    static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.name_tv)
        TextView name_tv;
        @BindView(R.id.flowlayout)
        TagFlowLayout flowlayout;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

    }

}
