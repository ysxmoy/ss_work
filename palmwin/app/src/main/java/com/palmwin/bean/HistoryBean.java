package com.palmwin.bean;

import java.io.Serializable;

/**
 *
 * Created by moy on 2018/4/2.
 */

public class HistoryBean implements Serializable {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
