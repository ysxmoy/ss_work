package com.palmwin.ui.activity;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.palmwin.palmwin.R;
import com.palmwin.ui.fragment.MainHistoryPageFragment;
import com.palmwin.ui.fragment.MainPlanPageFragment;

public class MainActivity extends AppCompatActivity {
    private static final int BOTTOM_PLAN_INDEX = 0;
    private static final int BOTTOM_HISTORY_INDEX = 1;
    private int mCurrentFragmentIndex = 0;
    private boolean mPlanFragmentAdded = false;
    private boolean mHistoryFragmentAdded = false;
    private MainPlanPageFragment mMainPlanPageFragment;
    private MainHistoryPageFragment mMainHistoryPageFragment;
    private BottomNavigationView navigation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        navigation = findViewById(R.id.navigation);
//        BottomNavigationHelper.disableShiftMode(navigation);

        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        if (savedInstanceState != null) {
            mCurrentFragmentIndex = savedInstanceState.getInt("mCurrentFragmentIndex");
        }

        switchFragment(mCurrentFragmentIndex);

    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_plan:
                    switchFragment(BOTTOM_PLAN_INDEX);
                    return true;
                case R.id.navigation_history:
                    switchFragment(BOTTOM_HISTORY_INDEX);
                    return true;
            }
            return false;
        }
    };

    private void switchFragment(int i) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();

        if (mCurrentFragmentIndex != i) {
            fragmentTransaction.hide(getFragment(mCurrentFragmentIndex));
        }

        switch (i) {
            case BOTTOM_PLAN_INDEX:
                if (mPlanFragmentAdded) {
                    fragmentTransaction.show(getFragment(i));
                } else {
                    fragmentTransaction.add(R.id.fl_content, getFragment(i));
                    mPlanFragmentAdded = true;
                }
                break;
            case BOTTOM_HISTORY_INDEX:
                if (mHistoryFragmentAdded) {
                    fragmentTransaction.show(getFragment(i));
                } else {
                    fragmentTransaction.add(R.id.fl_content, getFragment(i));
                    mHistoryFragmentAdded = true;
                }
                break;
        }
        fragmentTransaction.commitAllowingStateLoss();
        mCurrentFragmentIndex = i;
    }

    private Fragment getFragment(int menuItemId) {
        switch (menuItemId) {
            case BOTTOM_PLAN_INDEX:
                if (mMainPlanPageFragment == null) {
                    mMainPlanPageFragment = new MainPlanPageFragment();
                }
                return mMainPlanPageFragment;
            case BOTTOM_HISTORY_INDEX:
                if (mMainHistoryPageFragment == null) {
                    mMainHistoryPageFragment = new MainHistoryPageFragment();
                }
                return mMainHistoryPageFragment;

        }
        return null;
    }
}
