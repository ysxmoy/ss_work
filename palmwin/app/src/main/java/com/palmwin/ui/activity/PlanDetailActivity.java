package com.palmwin.ui.activity;

import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.palmwin.TabLayoutHelper;
import com.palmwin.adapter.MyFragmentAdapter;
import com.palmwin.palmwin.R;
import com.palmwin.ui.base.BaseActivity;
import com.palmwin.ui.fragment.PlanDetailFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 *
 * Created by moy on 2018/4/3.
 */

public class PlanDetailActivity extends BaseActivity {
    @BindView(R.id.tablayout)
    TabLayout mTablayout;
    @BindView(R.id.viewPager)
    ViewPager mViewPager;

    private ArrayList<Fragment> mFragments;
    private MyFragmentAdapter mPageAdapter;

    @Override
    public int getLayoutResId() {
        return R.layout.layout_plan_detail;
    }

    @Override
    public void initViews() {
        mToolbarIv.setVisibility(View.VISIBLE);
        mToolbarIv.setImageResource(R.drawable.search);
        List<String> titles = new ArrayList<>();
        titles.add("万位定码");
        titles.add("千位定码");
        titles.add("百位定码");
        mFragments = new ArrayList<>();
        for (int i = 0; i < titles.size(); i++) {
            PlanDetailFragment mPlanHotFragment = PlanDetailFragment.newInstance(titles.get(i));
            mFragments.add(mPlanHotFragment);
        }
        mPageAdapter = new MyFragmentAdapter(getSupportFragmentManager(), titles, mFragments);
        mViewPager.setAdapter(mPageAdapter);
        mTablayout.setupWithViewPager(mViewPager);
        TabLayoutHelper.reflex(mTablayout);
        mToolbarIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PlanDetailActivity.this, SearchActivity.class);
                startActivityForResult(intent,001);
            }
        });
    }


    @Override
    public String setTitle() {
        return "计划详情";
    }

    @Override
    public boolean isShowBack() {
        return true;
    }


}
