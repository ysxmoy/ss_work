package com.palmwin.ui.activity;


import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.palmwin.LogUtil;
import com.palmwin.palmwin.R;
import com.palmwin.ui.base.BaseActivity;
import com.palmwin.ui.view.DoubleSlideSeekBar;
import com.palmwin.ui.view.DoubleSlideSeekBar2;
import com.zhy.view.flowlayout.FlowLayout;
import com.zhy.view.flowlayout.TagAdapter;
import com.zhy.view.flowlayout.TagFlowLayout;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;

/**
 *
 * Created by moy on 2018/4/4.
 */

public class SearchActivity extends BaseActivity {
    @BindView(R.id.reald_tv)
    TextView mRealdTv;
    @BindView(R.id.period_tv)
    TextView mPeriodTv;
    @BindView(R.id.slideSeekbar1)
    DoubleSlideSeekBar mSlideSeekbar1;
    @BindView(R.id.slideSeekbar2)
    DoubleSlideSeekBar mSlideSeekbar2;
    @BindView(R.id.slideSeekbar3)
    DoubleSlideSeekBar mSlideSeekbar3;
    @BindView(R.id.slideSeekbar4)
    DoubleSlideSeekBar2 mSlideSeekbar4;
    @BindView(R.id.all_tv)
    TextView mAllTv;
    @BindView(R.id.invert_tv)
    TextView mInvertTv;
    @BindView(R.id.clear_tv)
    TextView mClearTv;
    @BindView(R.id.flowlayout)
    TagFlowLayout mFlowlayout;
    private ArrayList<String> mRandomList;
    private TagAdapter<String> mMTagAdapter;

    @Override
    public int getLayoutResId() {
        return R.layout.layout_search;
    }

    @Override
    public String setTitle() {
        return "搜索公式";
    }

    @Override
    public boolean isShowBack() {
        return true;
    }

    @Override
    public void initViews() {
        mSlideSeekbar1.setOnRangeListener(new DoubleSlideSeekBar.onRangeListener() {

            @Override
            public void onRange(int low, int big) {
                LogUtil.e("ysx", "mSlideSeekbar1" + low + "-------" + big);
            }
        });
        mSlideSeekbar2.setOnRangeListener(new DoubleSlideSeekBar.onRangeListener() {

            @Override
            public void onRange(int low, int big) {
                LogUtil.e("ysx", "mSlideSeekbar2" + low + "-------" + big);
            }
        });
        mSlideSeekbar3.setOnRangeListener(new DoubleSlideSeekBar.onRangeListener() {

            @Override
            public void onRange(int low, int big) {
                LogUtil.e("ysx", "mSlideSeekbar3" + low + "-------" + big);
            }
        });
        mSlideSeekbar4.setOnRangeListener(new DoubleSlideSeekBar2.onRangeListener() {

            @Override
            public void onRange(int low, int big) {
                LogUtil.e("ysx", "mSlideSeekbar3" + low + "-------" + big);
            }
        });
        mSlideSeekbar2.setCurrentVal(3,7);

        mRandomList = new ArrayList<>();
        mRandomList.add("0");
        mRandomList.add("1");
        mRandomList.add("2");
        mRandomList.add("3");
        mRandomList.add("4");
        mRandomList.add("5");
        mRandomList.add("6");
        mRandomList.add("7");
        mRandomList.add("8");
        mRandomList.add("9");

        mMTagAdapter = new TagAdapter<String>(mRandomList) {
            @Override
            public View getView(FlowLayout parent, int position, String s) {
                TextView tv = (TextView) LayoutInflater.from(SearchActivity.this).inflate(R.layout.tab_select_layout, mFlowlayout, false);
                tv.setText(s);
                return tv;
            }
        };
        mFlowlayout.setAdapter(mMTagAdapter);
    }


    @OnClick({R.id.reald_tv, R.id.period_tv, R.id.all_tv, R.id.invert_tv, R.id.clear_tv})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.reald_tv:
                break;
            case R.id.period_tv:
                break;
            case R.id.all_tv:
                break;
            case R.id.invert_tv:
                break;
            case R.id.clear_tv:
                break;
        }
    }
}
