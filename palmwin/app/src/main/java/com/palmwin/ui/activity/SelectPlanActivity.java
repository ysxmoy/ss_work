package com.palmwin.ui.activity;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.palmwin.IAdapterClickListener;
import com.palmwin.adapter.PlanSelectAdapter;
import com.palmwin.bean.HistoryBean;
import com.palmwin.palmwin.R;
import com.palmwin.ui.base.BaseListActivity;
import com.zhy.view.flowlayout.FlowLayout;
import com.zhy.view.flowlayout.TagAdapter;
import com.zhy.view.flowlayout.TagFlowLayout;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * Created by moy on 2018/4/3.
 */

public class SelectPlanActivity extends BaseListActivity {

    private ArrayList<HistoryBean> mDatas;
    private PlanSelectAdapter mAdapter;
    private LinearLayout mBannerLayout;
    private List<String> mRandomList;
    private TagFlowLayout mMIdFlowlayout;
    private TextView mNoData_tv;
    private TagAdapter mMTagAdapter;
    private ImageView mDelete_iv;

    @Override
    public void initView() {
        toolbarRight.setVisibility(View.VISIBLE);
        toolbarRight.setText("确定");
        mDatas = new ArrayList<>();
        mAdapter = new PlanSelectAdapter(this, mDatas);
//        recyclerView.addItemDecoration(new DefaultItemDecoration(getResources().getColor(R.color)));
        mBannerLayout = (LinearLayout) getLayoutInflater().inflate(R.layout.select_plan_head, null);
        mMIdFlowlayout = mBannerLayout.findViewById(R.id.flowlayout);
        mNoData_tv = mBannerLayout.findViewById(R.id.noData_tv);
        mDelete_iv = mBannerLayout.findViewById(R.id.delete_iv);
        recyclerView.addHeaderView(mBannerLayout);
        recyclerView.setAdapter(mAdapter);

        mRandomList = new ArrayList<>();
        mRandomList.add("地方大幅度");
        mRandomList.add("复古风格");
        mRandomList.add("复古风格");
        mRandomList.add("已很好看");
        mRandomList.add("已看");
        mRandomList.add("复古风格");
        mRandomList.add("复古风格");

        mMTagAdapter = new TagAdapter<String>(mRandomList) {
            @Override
            public View getView(FlowLayout parent, int position, String s) {
                TextView tv = (TextView) LayoutInflater.from(SelectPlanActivity.this).inflate(R.layout.tab_layout, mMIdFlowlayout, false);
                tv.setText(s);
                return tv;
            }
        };
        mMIdFlowlayout.setAdapter(mMTagAdapter);
        mMTagAdapter.notifyDataChanged();
        mMIdFlowlayout.setOnTagClickListener(new TagFlowLayout.OnTagClickListener() {
            @Override
            public boolean onTagClick(View view, int position, FlowLayout parent) {
                mRandomList.remove(position);
                mMTagAdapter.notifyDataChanged();
                if (mRandomList.size() == 0) {
                    mNoData_tv.setVisibility(View.VISIBLE);
                    mMIdFlowlayout.setVisibility(View.GONE);
                }
                return false;
            }
        });
        mAdapter.setIAdapterClickListener(new IAdapterClickListener() {
            @Override
            public void adapterClick(int id, int position, int position2) {
                if (id == 1) {
                    mRandomList.add("万位定位" + position + "--" + position2);
                } else {
                    if (mRandomList.size() > 0) {
                        mRandomList.remove(mRandomList.size() - 1);
                    }
                }
                mMTagAdapter.notifyDataChanged();
            }
        });
        mDelete_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mRandomList.clear();
                mMTagAdapter.notifyDataChanged();
                mAdapter.notifyDataSetChanged();
                mNoData_tv.setVisibility(View.VISIBLE);
                mMIdFlowlayout.setVisibility(View.GONE);
            }
        });
        getData();
    }

    private void getData() {
        for (int i = 0; i < 5; i++) {
            HistoryBean historyBean = new HistoryBean();
            historyBean.setName("定位系列" + i);
            mDatas.add(historyBean);
        }
        mAdapter.notifyDataSetChanged();
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void doRefresh() {
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void doLoadMore() {

    }

    @Override
    public String setTitle() {
        return "选择计划";
    }

    @Override
    public boolean isShowBack() {
        return true;
    }

}
