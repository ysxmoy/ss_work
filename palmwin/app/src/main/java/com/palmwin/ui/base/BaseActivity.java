package com.palmwin.ui.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.palmwin.palmwin.R;

import butterknife.ButterKnife;


public abstract class BaseActivity extends AppCompatActivity {

    public CoordinatorLayout coordinator;
    public ImageView mToolbarIv;
    private TextView mToolbarTitle;
	public TextView toolbarRight;
    public LinearLayout list_nodata;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);

        Toolbar toolbar = findViewById(R.id.toolbar);
        coordinator = findViewById(R.id.coordinator);

        mToolbarTitle = findViewById(R.id.toolbar_title);
        mToolbarIv = findViewById(R.id.toolbar_imageview);
        toolbarRight = findViewById(R.id.toolbar_right);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(isShowBack());


            toolbar.setBackgroundColor(getToolbarBgColor());
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.title_back_white);
            mToolbarTitle.setTextColor(getResources().getColor(android.R.color.white));

        // 不使用toolbar自带的title
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        mToolbarTitle.setText(setTitle());

        // 根据情况隐藏Toolbar
        if (!isShowToolbar()) {
            toolbar.setVisibility(View.GONE);
        }

        // activity视图
        View baseView = LayoutInflater.from(this).inflate(getLayoutResId(), null);

        // 绑定butterknife
        ButterKnife.bind(this, baseView);

        coordinator.addView(baseView, new CoordinatorLayout.LayoutParams(CoordinatorLayout.LayoutParams.MATCH_PARENT, CoordinatorLayout.LayoutParams.MATCH_PARENT));

        // 初始化控件
        initViews();
    }

    public abstract int getLayoutResId();

    public abstract String setTitle();

    public abstract boolean isShowBack();

    public abstract void initViews();

    public boolean isNeedChangeToolbarBg() {
        return false;
    }

    public int getToolbarBgColor() {
        return getResources().getColor(R.color.colorPrimary);
    }

    public boolean isShowToolbar() {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    /**
     * 显示一个文字Toast类型的消息
     *
     * @param msg 显示的消息
     */
    public void showToast(final String msg) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(BaseActivity.this, msg, Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void showSnackbar(final String msg) {
        Snackbar.make(coordinator, msg, Snackbar.LENGTH_LONG).show();
    }


    public void setToolbarTitle(String toolbarTitle) {
        mToolbarTitle.setText(toolbarTitle);
    }


/*    public void setBackColor(){
        mToolbarTitle.setTextColor(getResources().getColor(R.color.titleColor));
        toolbarRight.setTextColor(getResources().getColor(R.color.titleColor));
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.title_back_black);

    }*/
}
