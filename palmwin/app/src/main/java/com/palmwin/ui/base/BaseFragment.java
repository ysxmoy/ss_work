package com.palmwin.ui.base;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.palmwin.palmwin.R;
import com.yanzhenjie.recyclerview.swipe.SwipeMenuRecyclerView;

import butterknife.BindView;
import butterknife.ButterKnife;


public abstract class BaseFragment extends Fragment {

    public static final String TAG = BaseFragment.class.getSimpleName();

    @BindView(R.id.fragment_title)
    public TextView title;

    @BindView(R.id.recyclerView)
    public SwipeMenuRecyclerView recyclerView;

    @BindView(R.id.swipeRefreshLayout)
    public SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.toolbar_imageview)
    public ImageView mToolbarImageview;
    @BindView(R.id.imagleft)
    public ImageView mImagleft;
    @BindView(R.id.toolbar)
    RelativeLayout mToolbar;

    public BaseFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_base, container, false);
        ButterKnife.bind(this, view);
        initView();
        initViews(view);
        return view;
    }

    private void initView() {
        swipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.colorAccent));
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

            @Override
            public void onRefresh() {
                doRefresh();
            }
        });
        recyclerView.useDefaultLoadMore();
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        recyclerView.setLoadMoreListener(new SwipeMenuRecyclerView.LoadMoreListener() {
            @Override
            public void onLoadMore() {
                doLoadMore();
            }
        });
    }

    /**
     * 显示正在加载
     *
     * @param showLoading
     */
    public void showLoading(boolean showLoading) {
        if (swipeRefreshLayout.isRefreshing() && showLoading) return;
        swipeRefreshLayout.setRefreshing(showLoading);
    }

    public abstract void initViews(View view);

    public abstract void doRefresh();

    public abstract void doLoadMore();

    /**
     * 显示一个文字Toast类型的消息
     *
     * @param msg 显示的消息
     */
    public void showToast(final String msg) {
        if (getActivity() != null) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    public void hideToorBar() {
        mToolbar.setVisibility(View.GONE);
    }

}
