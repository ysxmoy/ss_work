package com.palmwin.ui.base;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;

import com.palmwin.palmwin.R;
import com.yanzhenjie.recyclerview.swipe.SwipeMenuRecyclerView;

import butterknife.BindView;



public abstract class BaseListActivity extends BaseActivity {

    @BindView(R.id.recyclerView)
    public SwipeMenuRecyclerView recyclerView;

    @BindView(R.id.swipeRefreshLayout)
    public SwipeRefreshLayout swipeRefreshLayout;


    @Override
    public int getLayoutResId() {
        return R.layout.fragment_base_no_title;
    }

    @Override
    public void initViews() {
        swipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.colorAccent));
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                doRefresh();
            }
        });

        recyclerView.useDefaultLoadMore();
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        recyclerView.setLoadMoreListener(new SwipeMenuRecyclerView.LoadMoreListener() {
            @Override
            public void onLoadMore() {
                doLoadMore();
            }
        });
        initView();
    }

    /**
     * 显示正在加载
     * @param showLoading
     */
    public void showLoading(boolean showLoading) {
        if (swipeRefreshLayout.isRefreshing() && showLoading) return;
        swipeRefreshLayout.setRefreshing(showLoading);
    }

    public abstract void initView();

    public abstract void doRefresh();

    public abstract void doLoadMore();

}
