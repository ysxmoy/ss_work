package com.palmwin.ui.fragment;


import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.palmwin.adapter.HistroyAdapter;
import com.palmwin.bean.HistoryBean;
import com.palmwin.palmwin.R;
import com.palmwin.ui.base.BaseFragment;
import com.yanzhenjie.recyclerview.swipe.widget.DefaultItemDecoration;

import java.util.ArrayList;

public class MainHistoryPageFragment extends BaseFragment {


    private ArrayList<HistoryBean> mDatas;
    private HistroyAdapter mAdapter;

    @Override
    public void initViews(View view) {
        title.setText("历史开奖");
        mDatas = new ArrayList<>();
        mAdapter = new HistroyAdapter(getContext(), mDatas);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.addItemDecoration(new DefaultItemDecoration(getResources().getColor(R.color.color_line)));
        recyclerView.setAdapter(mAdapter);
        getData();
    }

    private void getData() {
        for (int i = 0; i < 10; i++) {
            HistoryBean historyBean = new HistoryBean();
            historyBean.setName("2018-01-" + i);
            mDatas.add(historyBean);
        }
        mAdapter.notifyDataSetChanged();
        swipeRefreshLayout.setRefreshing(false);
        recyclerView.loadMoreFinish(false, true);

    }

    @Override
    public void doRefresh() {
        mDatas.clear();
        getData();
    }

    @Override
    public void doLoadMore() {
        getData();
        recyclerView.loadMoreFinish(false, true);
    }
}
