package com.palmwin.ui.fragment;


import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.palmwin.adapter.PlanAdapter;
import com.palmwin.bean.HistoryBean;
import com.palmwin.palmwin.R;
import com.palmwin.ui.activity.PlanDetailActivity;
import com.palmwin.ui.activity.SelectPlanActivity;
import com.palmwin.ui.base.BaseFragment;
import com.yanzhenjie.recyclerview.swipe.SwipeItemClickListener;
import com.yanzhenjie.recyclerview.swipe.widget.DefaultItemDecoration;

import java.util.ArrayList;

public class MainPlanPageFragment extends BaseFragment implements View.OnClickListener {


    private PlanAdapter mAdapter;
    private ArrayList<HistoryBean> mDatas;

    @Override
    public void initViews(View view) {
        title.setText("重庆时时彩");
        mToolbarImageview.setVisibility(View.VISIBLE);
        mImagleft.setVisibility(View.VISIBLE);
        mDatas = new ArrayList<>();
        mAdapter = new PlanAdapter(getContext(), mDatas);
        View mBannerLayout = getLayoutInflater().inflate(R.layout.plan_head_layout, null);
        View change_tv = mBannerLayout.findViewById(R.id.change_tv);
        View exchange_tv = mBannerLayout.findViewById(R.id.exchange_tv);
        change_tv.setOnClickListener(this);
        exchange_tv.setOnClickListener(this);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.addItemDecoration(new DefaultItemDecoration(getResources().getColor(R.color.color_line)));
        recyclerView.addHeaderView(mBannerLayout);
        recyclerView.setSwipeItemClickListener(new SwipeItemClickListener() {
            @Override
            public void onItemClick(View itemView, int position) {
                Intent intent = new Intent(getContext(), PlanDetailActivity.class);
                startActivity(intent);
            }
        });
        recyclerView.setAdapter(mAdapter);

        getData();
    }

    private void getData() {
        for (int i = 0; i < 5; i++) {
            HistoryBean historyBean = new HistoryBean();
            historyBean.setName("024-02" + i);
            mDatas.add(historyBean);
        }
        mAdapter.notifyDataSetChanged();
        swipeRefreshLayout.setRefreshing(false);
        recyclerView.loadMoreError(1, "善意提醒：小心参考，理想投资");


    }

    @Override
    public void doRefresh() {
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void doLoadMore() {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.change_tv:
                Intent intent = new Intent(getContext(), SelectPlanActivity.class);
                startActivityForResult(intent,001);
                break;
            case R.id.exchange_tv:
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }
}
