package com.palmwin.ui.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.text.Html;
import android.view.View;
import android.widget.TextView;

import com.palmwin.adapter.PlanDetailAdapter;
import com.palmwin.bean.HistoryBean;
import com.palmwin.palmwin.R;
import com.palmwin.ui.activity.SelectPlanActivity;
import com.palmwin.ui.base.BaseFragment;
import com.yanzhenjie.recyclerview.swipe.widget.DefaultItemDecoration;

import java.util.ArrayList;

public class PlanDetailFragment extends BaseFragment implements View.OnClickListener {


    private PlanDetailAdapter mAdapter;
    private ArrayList<HistoryBean> mDatas;
    private String planType;

    public static PlanDetailFragment newInstance(String planType) {
        Bundle args = new Bundle();
        args.putString("planType", planType);
        PlanDetailFragment fragment = new PlanDetailFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void initViews(View view) {
        hideToorBar();
        title.setText("重庆时时彩");
        mToolbarImageview.setVisibility(View.VISIBLE);
        mImagleft.setVisibility(View.VISIBLE);
        planType = getArguments().getString("planType");
        mDatas = new ArrayList<>();
        mAdapter = new PlanDetailAdapter(getContext(), mDatas);
        View mBannerLayout = getLayoutInflater().inflate(R.layout.plan_detail_head_layout, null);
        TextView text1 = mBannerLayout.findViewById(R.id.text1);
        String str1 = "准确率<font color='#E06268'>100%</font>";
        text1.setText(Html.fromHtml(str1));
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.addItemDecoration(new DefaultItemDecoration(getResources().getColor(R.color.color_line)));
        recyclerView.addHeaderView(mBannerLayout);
        recyclerView.setAdapter(mAdapter);
        getData();
    }

    private void getData() {
        for (int i = 0; i < 5; i++) {
            HistoryBean historyBean = new HistoryBean();
            historyBean.setName("024-02" + i);
            mDatas.add(historyBean);
        }
        mAdapter.notifyDataSetChanged();
        swipeRefreshLayout.setRefreshing(false);
        recyclerView.loadMoreError(1, "善意提醒：小心参考，理想投资");


    }

    @Override
    public void doRefresh() {
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void doLoadMore() {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.change_tv:
                Intent intent = new Intent(getContext(), SelectPlanActivity.class);
                startActivityForResult(intent, 001);
                break;
            case R.id.exchange_tv:
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }
}
