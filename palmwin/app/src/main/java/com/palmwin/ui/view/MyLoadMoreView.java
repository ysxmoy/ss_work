package com.palmwin.ui.view;

import com.yanzhenjie.recyclerview.swipe.SwipeMenuRecyclerView;

/**
 *
 * Created by moy on 2018/4/3.
 */

public class MyLoadMoreView implements SwipeMenuRecyclerView.LoadMoreView {
    @Override
    public void onLoading() {

    }

    @Override
    public void onLoadFinish(boolean dataEmpty, boolean hasMore) {

    }

    @Override
    public void onWaitToLoadMore(SwipeMenuRecyclerView.LoadMoreListener loadMoreListener) {
    }

    @Override
    public void onLoadError(int errorCode, String errorMessage) {

    }
}
